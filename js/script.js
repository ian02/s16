let count = 1;
while (count < 11){
	console.log ("count is " + count);
	count++
}

let num = 1;
while (num <= 10){
	console.log (num);
	num +=2;
}

//Practice 1
//display in the console all numbers between -10 and 19

let num1 = -9;
while (num1 < 19){
	console.log (num1);
	num1 ++
}


//Practice 2
//display all even numbers between 10 and 40

let num2 = 12;
while (num2 < 40){
	console.log (num2);
	num2 += 2
}


// Practice 3
//print all odd numbers between 300 and 333

let num3 = 301;
while (num3 < 333){
	console.log (num3);
	num3 += 2
}
